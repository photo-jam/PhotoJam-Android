package com.android.padjam.photojam;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.okhttp.OkHttpStack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {
    private static final int PREFERENCE_MODE_PRIVATE = 0;
    private static final String LOGIN_DETAILS = "Login";
    private static final int NUM_COLUMNS = 2;
    private static final int PERMISSION_REQUEST_CAMERA = 0;
    private static final int PERMISSION_ALL = 1;

    private static ArrayList<String> mImageUrls = new ArrayList<>();
    private static ArrayList<String> mNames = new ArrayList<>();
    static SharedPreferences preferenceLogin;
    final int REQUEST_IMAGE_CAPTURE = 1;
    final int PICK_IMAGE = 2;
    StaggeredRecyclerViewAdapter staggeredRecyclerViewAdapter;
    SwipeRefreshLayout swipeRefreshLayout;

    // Upload Photo Variables
    File mCurrentPhotoPath;
    File photoFile = null;
    static final int REQUEST_TAKE_PHOTO = 1;

    // FAB animation
    private Boolean isFabOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        final FloatingActionButton getPhoto = findViewById(R.id.get_photo);
        final FloatingActionButton camera = findViewById(R.id.camera);
        final FloatingActionButton gallery = findViewById(R.id.gallery);

        final Animation fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        final Animation fab_close = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        final Animation rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_forward);
        final Animation rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_backward);
        preferenceLogin = getSharedPreferences(LOGIN_DETAILS, PREFERENCE_MODE_PRIVATE);

        // Upload Service
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        OkHttpClient client = new OkHttpClient();
        UploadService.HTTP_STACK = new OkHttpStack(client);
        //Logger.setLogLevel(Logger.LogLevel.DEBUG);

        new ServerConnect(this).execute();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                staggeredRecyclerViewAdapter.clearData();
                new ServerConnect(MainActivity.this).execute();
            }
        });

        getPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isFabOpen){

                    getPhoto.startAnimation(rotate_backward);
                    camera.startAnimation(fab_close);
                    gallery.startAnimation(fab_close);
                    camera.setClickable(false);
                    gallery.setClickable(false);
                    isFabOpen = false;
                } else {
                    getPhoto.startAnimation(rotate_forward);
                    camera.startAnimation(fab_open);
                    gallery.startAnimation(fab_open);
                    camera.setClickable(true);
                    gallery.setClickable(true);
                    isFabOpen = true;
                }
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPhoto.startAnimation(rotate_backward);
                camera.startAnimation(fab_close);
                gallery.startAnimation(fab_close);
                camera.setClickable(false);
                gallery.setClickable(false);
                isFabOpen = false;
                String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                if(!hasPermissions(MainActivity.this, PERMISSIONS)){
                    ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, PERMISSION_ALL);
                } else {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        try {
                            photoFile = createImageFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if(photoFile != null){
                            Uri photoURI = FileProvider.getUriForFile(getApplicationContext(),"com.example.android.fileprovider",photoFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
                            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                        }
                    }
                }
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPhoto.startAnimation(rotate_backward);
                camera.startAnimation(fab_close);
                gallery.startAnimation(fab_close);
                camera.setClickable(false);
                gallery.setClickable(false);
                isFabOpen = false;
                Intent getPictureIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getPictureIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(getPictureIntent, "Select Picture"), PICK_IMAGE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String token = preferenceLogin.getString("access_token", "");
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            // Using Android Upload Service to upload image
            try {
                new MultipartUploadRequest(getApplicationContext(), "http://46.101.51.116/api/photo_upload")
                        // starting from 3.1+, you can also use content:// URI string instead of absolute file
                        .addFileToUpload(mCurrentPhotoPath.getPath(), "encoded_image")
                        .addHeader("Accept", "application/json")
                        .addHeader("Authorization", "Bearer " + token)
                        .setNotificationConfig(new UploadNotificationConfig())
                        .setMaxRetries(2)
                        .startUpload();
            } catch (Exception exc) {
                Log.e("AndroidUploadService", exc.getMessage(), exc);
            }
        } else if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (uri != null){
                try {
                    new MultipartUploadRequest(getApplicationContext(), "http://46.101.51.116/api/photo_upload")
                            .addFileToUpload(uri.toString(), "encoded_image")
                            .addHeader("Accept", "application/json")
                            .addHeader("Authorization", "Bearer " + token)
                            .setNotificationConfig(new UploadNotificationConfig())
                            .setMaxRetries(2)
                            .startUpload();
                } catch (Exception exc) {
                    Log.e("AndroidUploadService", exc.getMessage(), exc);
                }
            }

        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        mCurrentPhotoPath = image;
        return image;

    }

    private static class ServerConnect extends AsyncTask<Void, Void, String> {

        String reply;
        private WeakReference<MainActivity> activityReference;

        ServerConnect(MainActivity context){
            activityReference = new WeakReference<>(context);
        }
        protected String doInBackground(Void ... params) {
            String token = preferenceLogin.getString("access_token","");
            final String query = "http://46.101.51.116/api/photos";
            try {
                URL url = new URL(query);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Authorization","Bearer " + token);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);

                conn.connect();
                int response = conn.getResponseCode();
                String line;
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }
                    reply = sb.toString();
                } else {
                    reply = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return reply;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            final MainActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;

            if(s != null && !s.isEmpty()) {
                JSONObject jsonResponse;
                String name;
                String photo_url;
                try {
                    JSONArray response = new JSONArray(s);

                    for (int i = 0; i < response.length(); i++){
                        jsonResponse = response.getJSONObject(i);
                        name = jsonResponse.getString("face_name");
                        photo_url = jsonResponse.getString("path");
                        mNames.add(name);
                        mImageUrls.add(photo_url);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!mNames.isEmpty()) {
                    RecyclerView recyclerView = activity.findViewById(R.id.recyclerView);
                    activity.staggeredRecyclerViewAdapter =
                            new StaggeredRecyclerViewAdapter(activity.getApplicationContext(), mNames,mImageUrls);
                    StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(staggeredGridLayoutManager);
                    recyclerView.setAdapter(activity.staggeredRecyclerViewAdapter);
                    activity.staggeredRecyclerViewAdapter.setOnItemClickListener(new StaggeredRecyclerViewAdapter.ClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                            Bundle args = new Bundle();
                            args.putString("clicked_name", mNames.get(position));
                            Intent nameDisplayActivity;
                            nameDisplayActivity = new Intent(activity.getApplicationContext(), NameDisplayActivity.class);
                            nameDisplayActivity.putExtras(args);
                            activity.startActivity(nameDisplayActivity);
                        }
                    });
                }
                activity.swipeRefreshLayout.setRefreshing(false);
            }

        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}

