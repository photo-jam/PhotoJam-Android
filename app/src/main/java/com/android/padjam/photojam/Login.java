package com.android.padjam.photojam;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class Login extends AppCompatActivity {
    EditText user_name, password;
    TextView login_warning;
    Button log_in;
    private static final int PREFERENCE_MODE_PRIVATE = 0;
    private static final String LOGIN_DETAILS = "Login";
    SharedPreferences preferenceLogin;
    String CLIENT_SECRET = "hPR8lVO9x8vK75oGTkP2YBWaMzqz2KWAGeEQrr9n";
    private SharedPreferences.Editor preferenceLoginEditor;
    Intent intentActivity;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        intentActivity = new Intent(this, MainActivity.class);
        preferenceLogin = getSharedPreferences(LOGIN_DETAILS, PREFERENCE_MODE_PRIVATE);
        if (preferenceLogin.contains("username")) {
            startActivity(intentActivity);
            finish();
        }
        user_name = (EditText) findViewById(R.id.input_email);
        password = (EditText) findViewById(R.id.input_password);
        log_in = (Button) findViewById(R.id.btn_login);
        preferenceLoginEditor = preferenceLogin.edit();
        log_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(Login.this,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Authenticating...");
                progressDialog.show();
                new LoginRequest().execute();
            }
        });
    }

    private class LoginRequest extends AsyncTask<Void, Void, String> {

        String reply;
        @Override
        protected String doInBackground(Void... params) {
            //String json = "{\"username\":\"" + user_name.getText().toString() + "\"," + "\"password\":\"" + password.getText().toString() +  "\"}";
            final String query = "http://46.101.51.116/oauth/token";
            try {
                URL url = new URL(query);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("grant_type", "password")
                        .appendQueryParameter("client_id", "3")
                        .appendQueryParameter("client_secret", CLIENT_SECRET)
                        .appendQueryParameter("username", user_name.getText().toString())
                        .appendQueryParameter("password", password.getText().toString());
                String queryp = builder.build().getEncodedQuery();
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(queryp);
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                int response = conn.getResponseCode();
                String line;
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while((line = in.readLine()) != null){
                        sb.append(line);
                    }
                    reply = sb.toString();
                } else {
                    reply = null;
                }

            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            } catch (ProtocolException e1) {
                e1.printStackTrace();
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }


            return reply;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(s != null && !s.isEmpty()) {
                Log.i("string", "123" + s + "123");
                try {
                    JSONObject response = new JSONObject(s);
                    String token = response.getString("access_token");
                    preferenceLoginEditor.putString("access_token", token);
                    preferenceLoginEditor.putString("username", user_name.getText().toString());
                    preferenceLoginEditor.commit();
                    startActivity(intentActivity);
                    progressDialog.dismiss();
                    finish();
                    Log.i("token", token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
