package com.android.padjam.photojam;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SingleImageActivity extends AppCompatActivity {
    Intent thisIntent;
    private static final int PREFERENCE_MODE_PRIVATE = 0;
    private static final String LOGIN_DETAILS = "Login";
    SharedPreferences preferenceLogin;

    String file_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);
        ImageView image = findViewById(R.id.singleImage);
        final EditText nameChange = findViewById(R.id.changeName);
        Button nameSubmit = findViewById(R.id.submitName);
        preferenceLogin = getSharedPreferences(LOGIN_DETAILS, PREFERENCE_MODE_PRIVATE);
        thisIntent = getIntent();
        Bundle bundle = thisIntent.getExtras();
        String image_url = bundle.getString("image_url","");
        String name = bundle.getString("name","");
        file_name = bundle.getString("file_name","");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_launcher_background);
        Glide.with(this)
                .load(image_url)
                .apply(requestOptions)
                .into(image);
        nameChange.setHint("Not " + name + "? Tag the correct person.");
        nameSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String new_name = nameChange.getText().toString();
                new PostName().execute(new_name);
            }
        });
        // if name== unknown, say "Who is this?"
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class PostName extends AsyncTask<String, Void, String> {

        String reply;
        protected String doInBackground(String ... params) {
            String new_face = params[0];
            String token = preferenceLogin.getString("access_token","");
            final String query = "http://46.101.51.116/api/photos/" + file_name;
            try {
                URL url = new URL(query);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Authorization","Bearer " + token);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("name", new_face);
                String queryp = builder.build().getEncodedQuery();
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(queryp);
                writer.flush();
                writer.close();
                os.close();

                conn.connect();
                int response = conn.getResponseCode();
                String line;
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }
                    reply = sb.toString();
                } else {
                    reply = null;
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return reply;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s != null && !s.isEmpty()) {
                Log.d("SingleImageActivity","Reply: " + s);
            }
        }

    }
}
