package com.android.padjam.photojam;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;


public class StaggeredRecyclerViewAdapter extends RecyclerView.Adapter<StaggeredRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "StaggeredRecyclerViewAd";
    private static ClickListener clickListener;
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private Context mContext;

    StaggeredRecyclerViewAdapter(Context mContext, ArrayList<String> mNames, ArrayList<String> mImageUrls) {
        this.mNames = mNames;
        this.mImageUrls = mImageUrls;
        this.mContext = mContext;
    }

    @Override
    public @NonNull ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_grid_item, parent, false);
        //ViewHolder holder = new ViewHolder(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "OnBindViewHolder: called.");

        RequestOptions requestOptions = new RequestOptions();
        RequestOptions placeHolder = requestOptions.placeholder(R.drawable.ic_launcher_background);
        RequestOptions diskCache = requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        RequestOptions skipMem = requestOptions.skipMemoryCache(true);
        Log.i("RequestOptions","" + placeHolder + diskCache + skipMem);
        Glide.with(mContext)
                .load(mImageUrls.get(position))
                .apply(requestOptions)
                .into(holder.image);

        holder.name.setText(mNames.get(position));
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView image;
        TextView name;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.image = itemView.findViewById(R.id.imageview_widget);
            this.name = itemView.findViewById(R.id.name_widget);
        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "View clicked on");
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    void setOnItemClickListener(ClickListener clickListener) {
        StaggeredRecyclerViewAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    void clearData() {
        mNames.clear();
        mImageUrls.clear();
        this.notifyDataSetChanged();
    }
}
