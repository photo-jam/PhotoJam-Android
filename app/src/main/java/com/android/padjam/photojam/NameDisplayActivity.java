package com.android.padjam.photojam;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NameDisplayActivity extends AppCompatActivity {
    Intent thisIntent;

    private static final int PREFERENCE_MODE_PRIVATE = 0;
    private static final String LOGIN_DETAILS = "Login";
    private static final int NUM_COLUMNS = 2;

    StaggeredRecyclerViewAdapterNameDisplay staggeredRecyclerViewAdapterName;
    SharedPreferences preferenceLogin;
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mFileNames = new ArrayList<>();
    Intent singleNameDisplayActivity;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_display);
        thisIntent = getIntent();
        singleNameDisplayActivity = new Intent(this, SingleImageActivity.class);
        Bundle bundle = thisIntent.getExtras();
        name = bundle.getString("clicked_name","");
        setTitle(name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        preferenceLogin = getSharedPreferences(LOGIN_DETAILS, PREFERENCE_MODE_PRIVATE);
        new GetEntries().execute(name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetEntries extends AsyncTask<String, Void, String> {

        String reply;
        protected String doInBackground(String ... params) {
            String name = params[0];
            String token = preferenceLogin.getString("access_token","");
            final String query = "http://46.101.51.116/api/photos/" + name;
            try {
                URL url = new URL(query);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.setRequestProperty("Accept","application/json");
                conn.setRequestProperty("Authorization","Bearer " + token);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);

                conn.connect();
                int response = conn.getResponseCode();
                String line;
                if (response == HttpURLConnection.HTTP_OK) {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }
                    reply = sb.toString();
                } else {
                    reply = null;
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return reply;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(s != null && !s.isEmpty()) {
                JSONObject jsonResponse;
                String photo_url;
                String file_name;
                //image_urls = new ArrayList<String>();
                try {
                    JSONArray response = new JSONArray(s);

                    for (int i = 0; i < response.length(); i++){
                        jsonResponse = response.getJSONObject(i);
                        name = jsonResponse.getString("face_name");
                        photo_url = jsonResponse.getString("path");
                        file_name = jsonResponse.getString("file_name");
                        mFileNames.add(file_name);
                        mImageUrls.add(photo_url);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (!mFileNames.isEmpty()) {
                    RecyclerView recyclerViewName = findViewById(R.id.recyclerViewMain);
                    staggeredRecyclerViewAdapterName =
                            new StaggeredRecyclerViewAdapterNameDisplay(getApplicationContext(), mFileNames,mImageUrls);
                    StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
                    recyclerViewName.setLayoutManager(staggeredGridLayoutManager);
                    recyclerViewName.setAdapter(staggeredRecyclerViewAdapterName);
                    staggeredRecyclerViewAdapterName.setOnItemClickListener(new StaggeredRecyclerViewAdapterNameDisplay.ClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                            Bundle args = new Bundle();
                            args.putString("image_url", mImageUrls.get(position));
                            args.putString("name",name);
                            args.putString("file_name", mFileNames.get(position));
                            singleNameDisplayActivity.putExtras(args);
                            startActivity(singleNameDisplayActivity);
                        }
                    });
                }
            }

        }

    }
}
